import { createApp } from 'vue'

// 引入组件
import App from './App.vue'

// 引入第三方插件
import router from "./router.js";
import store from "./store.js";

// 创建
const app = createApp(App);

// 绑定 Vue-Router
app.use(router);

// 绑定 Vuex
app.use(store);

// 挂载 Vue 实例
app.mount('#app');
