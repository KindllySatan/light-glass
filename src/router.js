import { createRouter, createWebHistory } from "vue-router";

const indexPage = () => import('./pages/index/index.vue');

const routes = [
    {
        path: "/",
        redirect: "/index"
    },
    {
        path: '/index',
        name: 'index',
        component: indexPage
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
